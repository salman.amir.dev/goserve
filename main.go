package main

import (
	"flag"
	"fmt"

	"github.com/valyala/fasthttp"
)

var (
	dir  = flag.String("dir", "", "Directory to serve static files from")
	port = flag.Int("port", 3000, "TCP Port to serve file on")
)

func main() {
	flag.Parse()

	fs := &fasthttp.FS{
		Root:       *dir,
		IndexNames: []string{"index.html"},
	}

	fsHandler := fs.NewRequestHandler()
	requestHandler := func(ctx *fasthttp.RequestCtx) {
		fsHandler(ctx)
	}

	fmtAddr := fmt.Sprintf("localhost:%d", *port)
	errChan := make(chan error, 1)

	go func() {
		if err := fasthttp.ListenAndServe(fmtAddr, requestHandler); err != nil {
			errChan <- err
		}
	}()

	fmt.Printf("HTTP Server running on %s", fmtAddr)

	<-errChan
}
