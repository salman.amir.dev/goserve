build-all:
	echo "Compiling for every OS and Platform"
	# GOOS=freebsd GOARCH=amd64 go build -o bin/freebsd/goserve main.go
	GOOS=linux GOARCH=amd64 go build -o bin/goserve main.go
	GOOS=windows GOARCH=amd64 go build -o bin/windows/goserve main.go

build:
	GOOS=linux GOARCH=386 go build -o bin/goserve main.go