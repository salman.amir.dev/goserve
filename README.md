# GoServe - Simple yet fast File Server
### GoServe is a simple file server to assist frontend developers in testing their websites locally.
---
## **How to use**
### Copy the binary on your disk and add that path in your environment variable, to make it available globally. Now `cd` in to your frontend project and run `goserve` in the terminal.
---
## Configuration
### The `goserve` server defaults run on port `3000`. If you want to configure it to any other port, you can pass the `-port` parameter.
e.g
```bash
goserve -port=3005
```
---
## Caching issue
### Hard refresh the browser by pressing `Ctrl + Shift + R` on linux/windows or `Cmd + Shift + R` on Mac to clear the cache.